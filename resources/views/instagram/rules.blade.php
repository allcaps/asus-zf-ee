@extends('welcome')

@section('header')
    <div class="header">
        <a href="{{action('InstagramController@index')}}" class="header--button header--button__gallery">
            <span class="header--buttontext">Galerii</span>
            <em class="header--buttonem"></em>
        </a>
    </div>
    <div class="app--split">
        <h1 class="app--title">JUHEND</h1>
        <p class="app--subtitle">Kuidas mängus osaleda?</p>
    </div>
@stop

@section('content')
    <div class="app--content app--content__rules cf">
        <div class="app--rulesEntry app--rulesEntry__rule1"></div>
        <div class="app--rulesEntry app--rulesEntry__rule2"></div>
        <div class="app--rulesEntry app--rulesEntry__rule3"></div>
        <div class="app--rulesEntry app--rulesEntry__rule4"></div>
        <div class="app--winnerText">
            Võitjad kuulutatakse välja 25. juunil.<br/>
            Asus Eesti Facebook´i kontol.
        </div>
    </div>
@stop