@extends('welcome')

@section('header')
    <div class="header">
        <a href="{{action('InstagramController@rules')}}" class="header--button header--button__rules">
            <span class="header--buttontext">Juhend</span>
            <em class="header--buttonem"></em>
        </a>
    </div>
    <div class="app--split">
        <h1 class="app--title">GALERII</h1>
        <p class="app--subtitle">Hääleta enim meeldinud hulljulge seikluse eest!</p>
    </div>
@stop

@section('content')
    <div class="app--content">
        <div class="imagesContainer cf">
            @foreach($instagram_photos as $instagram_photo)
                <div class="imagesContainer--image img" data-src="{{ $instagram_photo->url }}" data-entry-id="{{ $instagram_photo->id }}">
                    <div class="imagesContainer--overlay imagesContainer--overlay__hidden">
                        <span class="imagesContainer--overlaycount counter">
                            {{ $instagram_photo->likes }}
                        </span>
                        <span class="imagesContainer--overlaytext">Teie hääl on vastu võetud</span>
                        <div class="imagesContainer--overlaycountlike"></div>
                    </div>
                    <div class="imagesContainer--like" data-id="{{ $instagram_photo->instagram_id }}">
                        <span class="imagesContainer--count counter">{{ $instagram_photo->likes }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop