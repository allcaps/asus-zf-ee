<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Asus Zenfone3</title>
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div class="app--container">
            @yield('header')
            @yield('content')
            <div class="app--footer">
                <div class="app--copyright">©2017 Asus ZenFone 3</div>
                <div class="app--agency">
                    <a href="http://www.allcaps.lt/" target="_blank">
                        Created by: ALL CAPS
                    </a>
                </div>
            </div>
        </div>

        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '1905585799699984',
                    cookie     : true,
                    xfbml      : true,
                    version    : 'v2.8'
                });
                FB.AppEvents.logPageView();
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <script src="{{ mix('/js/app.js') }}"></script>
    </body>
</html>
