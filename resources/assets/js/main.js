$(document).ready(function(){
	setTimeout(function(){
		FB.getLoginStatus(function(response) {
			if (response.status == 'connected') {
				window.fbLoggedIn = true;
				window.fbUser = response;
				$('#fb-login-button').hide();
				getVoted();
			}
		});
	},1000);

	$('#fb-login-button').on('click',getLoginStatus);
	$("div.img").unveil(1000);

	$('.imagesContainer--like').on('click',function(){
		window.voteId = $(this).attr('data-id');
		getLoginStatus();
	});
});

var ajar_url = 'https://game.zenfone3.ee/';

window.fbLoggedIn = false;
window.fbUser = false;
window.voteId = false;

function getVoted() {
	FB.api('/me', function (response) {
		window.fbLoggedIn = true;
		window.fbUser = response;
		$.ajax({
			url: ajar_url + 'get-voted',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: 'POST',
			dataType: 'json',
			data: {instagram_id: window.voteId, user_id: window.fbUser.id},
			success: function (result) {
				for (var i = 0; i < result.length; i++) {
					$('.imagesContainer--image[data-entry-id="' + result[i].instagram_photo_id + '"]').find('.imagesContainer--overlay').removeClass('imagesContainer--overlay__hidden');
					$('.imagesContainer--image[data-entry-id="' + result[i].instagram_photo_id + '"]').find('.imagesContainer--like').addClass('imagesContainer--like__hidden');
				}
			}
		});
	});
}

function getLoginStatus(){
	if(!window.fbLoggedIn) {
		FB.getLoginStatus(function (response) {
			if (response.status == 'connected') {
				window.fbLoggedIn = true;
				window.fbUser = response;
				$('#fb-login-button').hide();
			} else if (response.status === 'not_authorized') {
				fbLogin();
			} else {
				fbLogin();
			}
		});
	}else{
		FB.api('/me', function (response) {
			window.fbLoggedIn = true;
			window.fbUser = response;
			$.ajax({
				url: ajar_url + 'vote',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				dataType: 'json',
				data: {instagram_id: window.voteId, user_id: window.fbUser.id},
				success: function(result){
					if( "undefined" !== typeof result.success ){
						if(result.success){
							$('.imagesContainer--like[data-id="'+window.voteId+'"]').parent().find('.counter').each(function(){
								$(this).text(parseInt($(this).text()) + 1);
							});
							$('.imagesContainer--like[data-id="'+window.voteId+'"]').parent().find('.imagesContainer--overlay').removeClass('imagesContainer--overlay__hidden');
							$('.imagesContainer--like[data-id="'+window.voteId+'"]').addClass('imagesContainer--like__hidden');
						}else{

						}
					}
				}
			});
		});
	}
}

function fbLogin(){
	FB.login(function(response) {
		if (response.authResponse) {
			FB.api('/me', function (response) {
				window.fbLoggedIn = true;
				window.fbUser = response;
				$('#fb-login-button').hide();
			});
		}
	});
}