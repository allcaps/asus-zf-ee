<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instagram extends Model
{
    use SoftDeletes;
    
    protected $table = 'instagram';
    protected $dates = ['deleted_at'];
}
