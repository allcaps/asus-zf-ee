<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramVote extends Model
{
    protected $table = 'instagram_votes';
}
