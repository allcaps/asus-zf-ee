<?php

namespace App\Http\Controllers;

use App\Instagram;
use App\InstagramVote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InstagramController extends Controller
{
    public function index(){
        $instagram_photos = \App\Instagram::orderBy('id','desc')->get();

        return view('instagram.index',compact('instagram_photos'));
    }

    public function rules(){
        return view('instagram.rules');
    }

    public function vote(Request $request){
        if(empty($request->user_id) || empty($request->instagram_id)){
            return ['success' => false];
        }

        $user_agent = $request->server('HTTP_USER_AGENT');
        $user_ip = $request->server('REMOTE_ADDR');
        $instagram_id = $request->instagram_id;
        $user_id = $request->user_id;

        $photo = \App\Instagram::where(['instagram_id' => $instagram_id])->first();

        $entry = InstagramVote::where(['instagram_photo_id' => $photo->id, 'user_id' => $user_id])->first();
        if(!empty($entry)){
            return ['success' => false];
        }

        $photo->likes = $photo->likes + 1;
        $photo->save();

        $instagram_vote = new InstagramVote();
        $instagram_vote->instagram_photo_id = $photo->id;
        $instagram_vote->user_id = $user_id;
        $instagram_vote->ip_addess = $user_ip;
        $instagram_vote->user_agent = $user_agent;
        $instagram_vote->save();
        return ['success' => true];
    }

    public function getVoted(Request $request){
        if(empty($request->user_id)){
            return ['success' => false];
        }
        $user_id = $request->user_id;
        return InstagramVote::where(['user_id' => $user_id])->get(['instagram_photo_id']);
    }

    public function import(Request $request){
        if($request->allow_to_import != 'bTKZJ7Vy8HPUSLs3WFLvVUtu_6e9n9BpVajRzCShFs7SavN5J'){
            return '0';
        }
        $tag = $request->tag;
        $token = '2180667448.3a81a9f.0b7722523bdf4ae7896be8085205b609';
        $url = 'https://api.instagram.com/v1/tags/'.$tag.'/media/recent?access_token='.$token;
        $iterations = 0;

        $photos = [];
        $data = json_decode(file_get_contents($url));


        while($iterations < 80){
            $iterations++;

            foreach($data->data as $photo){
                $photos[] = $photo;
                $this->_saveImage($photo,$tag);
                unset($instagramPhoto);
            }

            $data = json_decode(file_get_contents($url));

            if(empty($data->pagination->next_url)){
                break;
            } else {
                $url = $data->pagination->next_url;
            }
        }
        $this->_cleanNotInList($tag);
        return '1';
    }

    private function _saveImage($photo,$tag){
        if($this->_checkIsPhotoImported($photo->id)){
            return false;
        }
        $instagramPhoto = new \App\Instagram();
        $instagramPhoto->instagram_id = $photo->id;
        $instagramPhoto->url = $photo->images->standard_resolution->url;
        $instagramPhoto->hashtag = $tag;
        $instagramPhoto->likes = 0;
        $instagramPhoto->in_list = 1;
        $instagramPhoto->save();
    }

    private function _cleanNotInList($tag){
        \App\Instagram::where(['in_list' => 0,'hashtag'=>$tag])->delete();
        DB::table('instagram')->update(array('in_list' => 0));
    }

    private function _checkIsPhotoImported($photo_instagram_id){
        $result = \App\Instagram::where('instagram_id','=',$photo_instagram_id)->first();
        if(empty($result)){
            return false;
        }else{
            $result->in_list = 1;
            $result->save();
            return true;
        }
    }

    public function getFeed(Request $request){
        $instagram_photos = \App\Instagram::orderBy('id','desc')->limit(12)->get();

        $payload = [];
        foreach ($instagram_photos as $photo) {
            $payload[] = [
                'id' => $photo->id,
                'url' => $photo->url,
                'likes' => $photo->likes,
                'hashtag' => $photo->hashtag,
            ];
        }

        return response()
            ->json(['payload' => $payload])
            ->withCallback($request->input('callback'));
    }
}
