<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram', function (Blueprint $table) {
            $table->increments('id');
            $table->string('instagram_id',40)->index('instagram_id');
            $table->text('url');
            $table->string('hashtag',20)->index('hashtag');
            $table->integer('likes')->index();
            $table->integer('in_list')->index();
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['instagram_id','hashtag']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram');
    }
}
