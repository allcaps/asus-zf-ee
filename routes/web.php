<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'InstagramController@index');
Route::post('/', 'InstagramController@index');
Route::get('/auth/', 'InstagramController@auth');
Route::get('/rules/', 'InstagramController@rules');
Route::post('/vote/', 'InstagramController@vote');
Route::post('/get-voted/', 'InstagramController@getVoted');
Route::get('/clean/', 'InstagramController@clean');
Route::get('/import/', 'InstagramController@import');
Route::get('/get-feed/', 'InstagramController@getFeed');